#!/bin/bash

set -e

# Default options
continent=africa
country=mali
tm_project=813
tm_url=https://tasking-manager-tm4-production-api.hotosm.org
tm_version=3

while getopts c:l:p:u:v: option
do
	case "${option}"
		in
		c) continent=${OPTARG};;
		l) country=${OPTARG};;
		p) tm_project=${OPTARG};;
		u) tm_url=${OPTARG};;
		v) tm_version=${OPTARG};;
	esac
done

# Build Tasking Manager project AoI download URL - TM version : $tm_version

if [ $tm_version = 3 ]
then
        tm_project_dl_url=$tm_url/api/v2/projects/$tm_project/tasks/?as_file=true
elif [ $tm_version = 2 ]
then
        tm_project_dl_url=$tm_url/project/$tm_project/tasks.json
fi


# Remove any '-' character from variable $country. Example:
#   $ country=burkina-faso
#   $ dbname=$( echo $country | sed -e 's/-//g' )
#   $ echo $dbname
#   burkinafaso
dbname=$( echo $country | sed -e 's/-//g' )

work_base_dir=/data/work/
work_dir=$work_base_dir/$(date +%Y-%m-%d)/$country/$tm_project/

mkdir -p $work_dir

echo ------------------------------------------------------
echo Clean oldest working dirs, keep only the 4 newest ones.
echo

ls -1trd $work_base_dir/* | head -n -4 | xargs rm -rf

echo ------------------------------------------------------
echo Get OSM data for $country
echo

wget \
	http://download.geofabrik.de/$continent/$country-latest.osm.pbf \
	--output-document $work_dir/$country-latest.osm.pbf


echo ------------------------------------------------------
echo Create DB $dbname if not exists with postgis extension
echo

psql -U postgres -tc "SELECT 1 FROM pg_database WHERE datname='$dbname';" | grep -q 1 || psql -U postgres -ac "CREATE DATABASE $dbname;"

psql -U postgres -d $dbname -ac "CREATE EXTENSION IF NOT EXISTS postgis;"

echo ------------------------------------------------------
echo Upload roads into the PostGIS database
echo

/opt/imposm/imposm import -config /data/scripts/config.json -read $work_dir/$country-latest.osm.pbf -connection postgis://postgres@localhost/$dbname?prefix=NONE -overwritecache -write
/opt/imposm/imposm import -config /data/scripts/config.json -connection postgis://postgres@localhost/$dbname?prefix=NONE -deployproduction -removebackup

echo ------------------------------------------------------
echo Get the Tasking Manager project AoI as geoJSON at $tm_project_dl_url
echo

wget \
	$tm_project_dl_url \
	--output-document $work_dir/tasking-manager-$tm_project.geojson


echo ------------------------------------------------------
echo Upload Tasking Manager project into the PostGIS database
echo

psql -U postgres -d $dbname -a << EOF 
DROP TABLE IF EXISTS tasks_hotosm_$tm_project;
EOF

su postgres -c "ogr2ogr -f PostgreSQL PG:"dbname=$dbname" $work_dir/tasking-manager-$tm_project.geojson -nln "tasks_hotosm_$tm_project" -preserve_fid"

echo ------------------------------------------------------
echo Compute intersections between OSM roads buffers and Tasking Manager tasks
echo

psql -U postgres -d $dbname -a -f /data/scripts/compute_intersections.sql
psql -U postgres -d $dbname -a << EOF 
DROP SCHEMA IF EXISTS process CASCADE;
CREATE SCHEMA process;
DROP SCHEMA IF EXISTS result CASCADE;
CREATE SCHEMA result;

CREATE TABLE process.roads_buffer 
AS (
SELECT public.ST_Buffer(way,500),osm_id 
FROM public.roads_gen0
);

CREATE TABLE process.roads_buffer_merged 
AS ( 
SELECT public.ST_Multi(public.ST_Union(f.st_buffer)) 
AS singlegeom 
FROM process.roads_buffer  AS f
);

CREATE INDEX roads_buffer_merged_way_idx 
ON process.roads_buffer_merged
USING GIST (singlegeom);

CREATE TABLE process.tasks_hotosm_$tm_project AS SELECT * FROM public.tasks_hotosm_$tm_project; --limit 1;--utile pour tester rapidement

CREATE INDEX tasks_hotosm_${tm_project}_geom_idx 
ON process.tasks_hotosm_$tm_project
USING GIST (wkb_geometry);

ALTER TABLE process.tasks_hotosm_$tm_project RENAME COLUMN ogc_fid TO taskid;

--if tm_version=2: rename column 'state' to 'taskstatus'
DO \$\$
BEGIN 
IF $tm_version=2 THEN 
ALTER TABLE process.tasks_hotosm_$tm_project RENAME COLUMN state TO taskstatus;
ALTER TABLE process.tasks_hotosm_$tm_project ALTER COLUMN taskstatus SET DATA TYPE varchar;
UPDATE process.tasks_hotosm_$tm_project SET taskstatus='READY' WHERE taskstatus='0';
UPDATE process.tasks_hotosm_$tm_project SET taskstatus='MAPPED' WHERE taskstatus='2';
UPDATE process.tasks_hotosm_$tm_project SET taskstatus='VALIDATED' WHERE taskstatus='3';
END IF;
END \$\$;

ALTER TABLE process.tasks_hotosm_$tm_project ADD COLUMN percent_intersect integer;

UPDATE process.tasks_hotosm_$tm_project SET percent_intersect = (
SELECT 100*ST_AREA(ST_INTERSECTION(wkb_geometry, ST_TRANSFORM(t.singlegeom,4326)))/ST_AREA(wkb_geometry)
) FROM process.roads_buffer_merged  AS t;


DROP TABLE IF EXISTS process.tasks_hotosm_${tm_project}_buffer;
CREATE TABLE process.tasks_hotosm_${tm_project}_buffer
AS (
SELECT   
taskid,
taskstatus,
percent_intersect,
public.ST_Buffer(wkb_geometry,0.02) AS geom
FROM process.tasks_hotosm_$tm_project
);

ALTER TABLE process.tasks_hotosm_${tm_project}_buffer ADD COLUMN surface float;

UPDATE process.tasks_hotosm_${tm_project}_buffer SET surface = (
SELECT ST_AREA(geom, false)
);

ALTER TABLE process.tasks_hotosm_${tm_project}_buffer ADD COLUMN url varchar;


--if tm_version=2: rename column 'state' to 'taskstatus'
DO \$\$
BEGIN
IF $tm_version=2 THEN
UPDATE process.tasks_hotosm_${tm_project}_buffer SET url = (
SELECT CONCAT(
'$tm_url',
'/project/'
'$tm_project',
'#task/',
"taskid")
);
ELSEIF $tm_version=3 THEN
UPDATE process.tasks_hotosm_${tm_project}_buffer SET url = (
SELECT CONCAT(
'$tm_url',
'/project/',
'$tm_project',
'?task=',
"taskid")
);
END IF;
END \$\$;

CREATE TABLE  result.tasks_hotosm_${tm_project}_buffer_0_10 AS (
SELECT * FROM  process.tasks_hotosm_${tm_project}_buffer WHERE percent_intersect <= 10 AND taskstatus='READY'
);
CREATE TABLE  result.tasks_hotosm_${tm_project}_buffer_10_30 AS (
SELECT * FROM  process.tasks_hotosm_${tm_project}_buffer WHERE percent_intersect > 10 AND percent_intersect <= 30 AND taskstatus='READY'
);
CREATE TABLE  result.tasks_hotosm_${tm_project}_buffer_30_60 AS (
SELECT * FROM  process.tasks_hotosm_${tm_project}_buffer WHERE percent_intersect > 30 AND percent_intersect <= 60 AND taskstatus='READY'
);
CREATE TABLE  result.tasks_hotosm_${tm_project}_buffer_60_100 AS (
SELECT * FROM  process.tasks_hotosm_${tm_project}_buffer WHERE percent_intersect > 60 AND taskstatus='READY'
);
CREATE TABLE  result.tasks_hotosm_${tm_project}_buffer_mapped AS (
SELECT * FROM  process.tasks_hotosm_${tm_project}_buffer WHERE taskstatus='MAPPED' OR taskstatus='VALIDATED'
);
EOF


echo ------------------------------------------------------
echo Produce results as geoJSON 
echo

rm -f /tmp/tasks_hotosm_${tm_project}*
rm -f /data/result/$tm_project/*
mkdir -p /data/result/$tm_project

# Write geojson in a place user postgres can write (/tmp)...
su postgres -c "ogr2ogr -f "GeoJSON" /tmp/tasks_hotosm_${tm_project}_buffer_0_10.geojson PG:"dbname=$dbname" "result.tasks_hotosm_${tm_project}_buffer_0_10""
su postgres -c "ogr2ogr -f "GeoJSON" /tmp/tasks_hotosm_${tm_project}_buffer_10_30.geojson PG:"dbname=$dbname" "result.tasks_hotosm_${tm_project}_buffer_10_30""
su postgres -c "ogr2ogr -f "GeoJSON" /tmp/tasks_hotosm_${tm_project}_buffer_30_60.geojson PG:"dbname=$dbname" "result.tasks_hotosm_${tm_project}_buffer_30_60""
su postgres -c "ogr2ogr -f "GeoJSON" /tmp/tasks_hotosm_${tm_project}_buffer_60_100.geojson PG:"dbname=$dbname" "result.tasks_hotosm_${tm_project}_buffer_60_100""
su postgres -c "ogr2ogr -f "GeoJSON" /tmp/tasks_hotosm_${tm_project}_buffer_mapped.geojson PG:"dbname=$dbname" "result.tasks_hotosm_${tm_project}_buffer_mapped""

# ... then move them as root in the final destination (which is not writeable by
# user postgres).
mv /tmp/tasks_hotosm_${tm_project}* /data/result/$tm_project/
