osmtrax is a tool to compute some estimation of the progress of a road network
consolidation project in an OpenStreetMap Tasking Manager instance (like
[project #813](https://tasks.hotosm.org/project/813) for the Mali road
network).

It produces some geojson files identifying tasks where there are few roads in
OSM. In fact it computes some intersection ratio between the task geographical
region and roads that already exist in this region.

One can use the geojson files in an GIS display, like [this uMap for Mali](http://u.osmfr.org/m/232379/).

# How to use

First clone this repository:

```sh
git clone https://framagit.org/nnodot/osmtrax
```

Build the docker image:
```sh
docker build --tag osmtrax docker
```
This docker image recompile GDAL wich is a long process.

Create the docker container:
```sh
docker run \
  --detach \
  --env POSTGRES_PASSWORD='some_password' \
  --volume $(pwd):/data \
  --publish 5432:5432 \
  --name osmtrax \
  osmtrax
```
Only use the `--publish` option, which maps ports, if you need/want to access
the database from outside the container.

Start the container:
```sh
docker start osmtrax
```

Now you need to chose what country to process. A project in the Tasking
Manager for road network consolidation must exist. Prepare some parameters:
* `-c` and `-l`: respectively the continent of the country to process and the the country to process. The country and continent appear in the country URL on the Geofabrik download server. For example if the URL is `http://download.geofabrik.de/africa/burkina-faso.html` its continent is `africa` and its country is `burkina-faso`. You must respect spelling and case. Default is are `africa` and `mali`.
* `-p` the number of the project in the HOT Tasking Manger. This number
appears at the end of the URL of the project. For example if the URL is
`https://tasks.hotosm.org/project/813` its number is 813. Default is `813`.
* `-u` the url of the TM instance used. Default is `https://tasks.hotosm.org`.
* `-v` the version of the TM instance used. Default is `3`.

Then, execute the computation of the road intersection:
```sh
docker exec osmtrax sh /data/scripts/compute_intersections.sh -c africa -l mali -p 813 -u 'https://tasks.hotosm.org' -v 3 
```

This will produce some geojson files in the `results/$p/` directory. For example,
with the default parameters values, `results/813/` will contain:

```
tasks_hotosm_813_buffer_0_10.geojson
tasks_hotosm_813_buffer_10_30.geojson
tasks_hotosm_813_buffer_30_60.geojson
tasks_hotosm_813_buffer_60_100.geojson
tasks_hotosm_813_buffer_mapped.geojson
```

# Advanced topics

## Filter OSM roads to use

One can filter the data to use from OSM before the intersection computation.
Use the `scripts/mapping-main-roads.json` file to define what to filter. This
file is used by `imposm` to import data. See the [official imposm
documentation](https://imposm.org/docs/imposm3/latest/mapping.html) to get
instructions on how to filter data.

## Change the buffer size

To do the intersection computation we apply a buffer on the roads. The size of
the buffer must be the same as the one used to create the geographical regions
in the HOT Tasking Manager project. Ususaly this value is 500. If it is not the
case, one has to edit the SQL script in file `scripts/compute_intersections.sh`
to change this value (look for the appropriate call to `ST_Buffer`).
